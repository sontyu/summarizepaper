import pymsteams, os

def send_text(title, text):
    myteamsmessage = pymsteams.connectorcard(os.getenv("TEAMS_CLIENT"))
    myteamsmessage.title(title)
    myteamsmessage.text(text)
    myteamsmessage.send()