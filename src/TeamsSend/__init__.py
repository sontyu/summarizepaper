__version__      = '0.0.1'
__author__       = 'sontyu'
__author_email__ = 'sontyu.parsonal.address@gmail.com'
__url__          = 'https://gitlab.com/sontyu/summarizepaper.git'

__all__ = [
    "send_text"
]

from .send_message import send_text
