import datetime
import arxiv

def search_query(query, max_results):
    search = arxiv.Search(
        query=query,
        max_results=max_results,
    )
    results_list = []
    for result in arxiv.Client().results(search):
        results_list.append(result)
    return results_list

def get_paper_summary(result: arxiv.Result):
    text = f"title: {result.title}\nbody: {result.summary}\nurl{result.entry_id}"
    return text
