from openai import AzureOpenAI
from .config import get_client
from .query import get_system_query

def get_response(system_content, user_content):
    client = get_client()
    response = client.chat.completions.create(
        model="gpt-35-turbo", # model = "deployment_name".
        messages=[
            {"role": "system", "content": system_content},
            {"role": "user", "content": user_content}
        ]
    )
    return response.choices[0].message.content

def get_doc_sum(doc):
    system = get_system_query()
    return get_response(system, doc)
    