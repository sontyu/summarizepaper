__version__      = '0.0.1'
__author__       = 'sontyu'
__author_email__ = 'sontyu.parsonal.address@gmail.com'
__url__          = 'https://gitlab.com/sontyu/summarizepaper.git'

__all__ = [
    "get_doc_sum"
]

from .get_response import get_doc_sum