from ArxivLoader import get_paper_summary, search_query
from SumPaper import get_doc_sum
from TeamsSend import send_text
from search_query import search_query_list
import datetime

def main(max_results=5):
    bar = "\n---\n\n"
    for query in search_query_list:
        out_text = "検索内容:{} \n\n".format(query)
        out_text += bar
        res_list=search_query(query,max_results=max_results)
        for res in res_list:
            text = get_paper_summary(res)
            response = get_doc_sum(text)
            out_text += response
            out_text += bar
        now = datetime.datetime.now()
        send_text(now, out_text)

if __name__ == "__main__":
    main()

        


