# SummarizePaper
arXivで出された最新論文をチェック、GPTで要約を実行してTeamsで送信を実行する。

## 事前準備
### 設定情報を入力
`.sumpaper/.env`に環境変数を記載。

```.env
AZURE_OPENAI_ENDPOINT=""
AZURE_OPENAI_KEY=""
API_VERSION="2023-05-15"
TEAMS_CLIENT=""
```
必要な情報は`AZURE_OPENAI_ENDPOINT`, `AZURE_OPENAI_KEY`, `TEAMS_CLIENT`を取得しておくこと。

azure openaiに関する情報は[ここ](https://learn.microsoft.com/ja-jp/azure/ai-services/openai/)を参考に取得してください。

tesmsのクライアントは[ここ](https://learn.microsoft.com/ja-jp/microsoftteams/platform/webhooks-and-connectors/how-to/add-incoming-webhook?tabs=dotnet)を参考にして取得してください。

### 検索したいコンテンツを変更
`src/search_query.py`に検索したい内容を入力しておく

```python 
search_query_list = [
    "AI", #ここに検索したい内容を追加していく
    ]
```

## 環境構築
docker composeで環境構築を行う
```bash
$ cd .sumpaper
$ docker compose up -d
```


## 実行
```bash
$ docker exec -it openai python src/main.py 
```

これで論文調査を比較的楽に行えるようになりました！

